class System {
    constructor(A, n, B, p, C, q, D, T, x_0, K, LQR, OFB, G, H, kalman, L, Q, R, r) {
        this.n = n;
        this.p = p;
        this.q = q;
        this.A = A;
        this.B = B;
        this.C = C;
        this.D = D;
        this.x_0 = x_0;
        this.r = r;
        this.T = T;
        this.Q = Q;
        this.R = R;
        this.LQR = LQR;
        this.OFB = OFB;
        this.L = L;
        this.G = G;
        this.H = H;

        this.KF = {on: kalman};
        this.reset();

        this.approximationOrder = 25;
        this.A_d = approxTransitionMatrix(this.A, this.T, this.n, this.approximationOrder);
        this.B_d = calculateBd(this.A, this.A_d, this.B, this.n, this.T);


        if (this.LQR) {
            this.K = DLQR(this.A_d, this.B_d, this.Q, this.R, this.n);
        } else {
            this.K = K;
        }

        let temp = math.add(math.multiply(math.subtract(this.C, math.multiply(this.D, this.K)),  // Turn into function!!
                   math.multiply(math.inv(math.subtract(math.multiply(this.B, this.K), this.A)), this.B)), this.D);
        try {
            this.K_r = math.inv(temp);  // This achieves the condition y->r as t->inf
        }
        catch {  // If inverse does not exist, try Moore-Penrose pseudoinverse instead
            this.K_r = pseudoInverse(temp);  // turn into pseudoinverse function!!
        }

        this.u = [0];

        console.log("system = ", this);
    }

    initObserver(on) {
        this.KF = {
            on: on,
            K: [],
            P_apri: [],
            P_apost: []
        };

        if (this.OFB) {
            this.x_est_apri.push(math.zeros(this.n, 1));
        }

        if (on) {
            this.KF.P_apri.push(math.identity(this.n));
            this.updateKalmanGain();
            this.updateErrorCovariance();
        }
    }

    reset() {
        this.time = 0;
        this.x = [math.clone(this.x_0)];
        this.x_k = math.clone(this.x_0);
        this.y = [];
        this.x_est = [];
        this.x_est_apri = [];
        this.initObserver(this.KF.on);
        this.updateY();
        if (this.OFB) this.updateXest();
    }

    updateX() {
        this.u = math.subtract(math.multiply(this.K_r, this.r), math.multiply(this.K, this.x_k));
        this.x_k = math.add(math.multiply(this.A_d, this.x_k), math.multiply(this.B_d, this.u));

        if (this.OFB) {
            this.x_k = math.add(this.x_k, math.multiply(this.G, randomN(1, 0)));
        }
        this.x.push(math.clone(this.x_k));
        this.time += this.T;
    }

    updateY() {
        if (this.OFB) {
            this.y.push(math.add(math.multiply(this.C, this.x_k), math.multiply(this.H, randomN(1, 0))));
        } else {
            this.y.push(math.multiply(this.C, this.x_k));
        }
    }

    updateKalmanGain() {
        let P_apri = this.KF.P_apri[this.KF.P_apri.length - 1];
        this.L =
            math.multiply(  // Update Kalman gain
                P_apri,
                math.transpose(this.C),
                math.inv(
                    math.add(
                        math.multiply(
                            this.C,
                            P_apri,
                            math.transpose(this.C)),
                        math.multiply(this.H, math.transpose(this.H))  //HRH^T here
                    )
                )
            );

        this.KF.K.push(math.clone(this.L));
    }

    updateErrorCovariance() {
        let P_apri = this.KF.P_apri[this.KF.P_apri.length - 1];
        this.KF.P_apost.push(
                math.multiply(
                    math.subtract(
                        math.identity(this.n, this.n),
                        math.multiply(
                            this.L,
                            this.C
                        )
                    ),
                    P_apri
            )
        );
    }

    projectAhead() {
        this.x_est_apri.push( // project ahead and estimate x (and P) without measurement
            math.add(
                math.multiply(
                    this.A_d,
                    this.x_est[this.x_est.length - 1]),
                math.multiply(
                    this.B_d,
                    this.u
                )
            )
        );

        if (this.KF.on) {
            this.KF.P_apri.push(
                math.add(
                    math.multiply(
                        this.A_d,
                        this.KF.P_apost[this.KF.P_apost.length - 1],
                        math.transpose(this.A_d)),
                    math.multiply(this.G, math.transpose(this.G)) // GQG^T here
                )
            );

        }
    }

    updateXest() {  // Update estimate with measurement
        let x_est_apri = this.x_est_apri[this.x_est_apri.length - 1];
        this.x_est.push(
            math.add(
                x_est_apri,
                math.multiply(
                    this.L,
                    math.subtract(
                        this.y[this.y.length - 1],
                        math.multiply(
                            this.C,
                            x_est_apri  // Missing D*u here
                        )
                    )
                )
            )
        );
    }
}

function approxTransitionMatrix (A, T, n, order) {  // Approximates the transition matrix from A with sample time T, to degree n
    let A_d = math.identity(n);
    if (order > 1) {
        for (let i = 1; i <= order; ++i) {
            A_d = math.add(A_d, math.multiply(math.pow(A, i), math.pow(T, i) / math.factorial(i)));
        }
    }
    return A_d;
}

function calculateBd(A, A_d, B, n, T) {
    try {  // A not always singular
        return math.multiply(math.inv(A), math.subtract(A_d, math.identity(n)), B);
    }
    catch(err) {
        let I = math.multiply(T, math.identity(n)); // Series expansion approximation

        for (let i = 1; i < 20; ++i) {
            I = math.add(I, math.multiply(math.pow(T, i+1) / math.factorial(i+1), math.pow(A, i)));
        }
        return math.multiply(I, B);
    }
}

function DLQR (Ad, Bd, Q, R, n) {
    let N = math.zeros(2*n, 2*n);
    N = N.subset(math.index(math.range(0,n), math.range(0,n)), Ad);
    N = N.subset(math.index(math.range(n,2*n), math.range(0,n)), math.multiply(-1, Q));
    N = N.subset(math.index(math.range(n,2*n), math.range(n,2*n)), math.identity(n));

    let L = math.zeros(2*n, 2*n);
    L = L.subset(math.index(math.range(0,n), math.range(0,n)), math.identity(n));
    L = L.subset(math.index(math.range(0,n),math.range(n,2*n)), math.multiply(Bd, math.inv(R), math.transpose(Bd)));
    L = L.subset(math.index(math.range(n, 2*n), math.range(n, 2*n)), math.transpose(Ad));

    let A = math.subtract(N, L);
    let B = math.add(N, L);

    let H = math.multiply(math.inv(B), A);

    let P = [];
    let V = [];
    P.push(math.pow(math.multiply(math.inv(B), A), 2));
    V.push(math.identity(2*n));
    for (let i = 1; i < 30; ++i) {
        P[i] = math.multiply(1/2, math.add(P[i-1], math.inv(V[i-1])));
        V[i] = math.multiply(1/2, math.add(V[i-1], math.inv(P[i-1])));
    }
    let X12 = math.subtract(H, P[P.length - 1]);
    let X1 = X12.subset(math.index(math.range(0,n), math.range(0,n)));
    let X2 = X12.subset(math.index(math.range(n, 2*n), math.range(0,n)));
    let X = math.multiply(X2, math.inv(X1));
    let K = math.multiply(math.inv(math.add(R, math.multiply(math.transpose(Bd), X, Bd))), math.multiply(math.transpose(Bd), X, Ad));
    return K;
}

function randomN(sigma, mu) {
    var x = Math.random();
    var y = Math.random();
    return Math.sqrt(-2 * Math.log(x)) * Math.cos(2 * Math.PI * y) * sigma + mu;
}

function pseudoInverse(A) {
    return math.multiply(math.inv(math.multiply(math.transpose(A), A)), math.transpose(A));
}