function generateMatrixInput(n, m, M) {
    $("#" + M).empty();
    for (let i = 0; i < n; ++i) {
        for (let j = 0; j < m; ++j) {
            $("#" + M).append('<input type="number" class="form-control input-matrix" id="' + M + '-' + i + '-' + j + '">');
        }
        $("#" + M).append('<br>');
    }
}

function generateMatrixInputs(n, p, q) {
    generateMatrixInput(n, n, "A");
    generateMatrixInput(n, p, "B");
    generateMatrixInput(q, n, "C");
    generateMatrixInput(n, 1, "x0");
    generateMatrixInput(q, p, "D");
    generateMatrixInput(p, n, "K");
    generateMatrixInput(q, 1, "r");
    generateMatrixInput(n, n, "Q");
    generateMatrixInput(p, p, "R");
    generateMatrixInput(n, q, "L");
    generateMatrixInput(n, 1, "G");
    generateMatrixInput(q, 1, "H");
}

function generatePlotForm(system) {
    $("#state-time-plot-checks").empty();
    $("#trajectory-x").empty();
    $("#trajectory-x").append('<option value=-1>0</option>');
    $("#trajectory-y").empty();
    $("#trajectory-y").append('<option value=-1>0</option>');
    for (let i = 0; i < system.n; ++i) {
        $("#state-time-plot-checks").append('<div class="form-check mr-2 d-inline"><input type="checkbox" class="form-check-input" id="state-time-plot-check-' + i + '" checked><label class="form-check-label">x' + i + '</label></div>');
        $("#trajectory-x").append('<option value="' + i + '">x' + i + '</option>');
        $("#trajectory-y").append('<option value="' + i + '">x' + i + '</option>');
    }
}

function initContext(canvasID, contextType) {
    var canvas = document.getElementById(canvasID);
    var context = canvas.getContext(contextType);
    return context;
}

function fillInput(A, A_name, n, m) {
    for (var i = 0; i < n; ++i) {
        for (var j = 0; j < m; ++j) {
            $("#" + A_name + '-' + i + '-' + j).val(A[i][j]);
        }
    }
}

function fillInputs(option) {
    let n = 0;
    let p = 0;
    let q = 0;
    let T = 0;
    let A = [];
    let B = [];
    let C = [];
    let D = [];
    let K = [];
    let x_0 = [];
    let r = [];

    if (option === 1) {  // Mass spring damper
        n = 2;
        p = 1;
        q = 1;
        T = 0.05;
        A = [[0, 1], [-3, -1]];
        B = [[0], [1]];
        C = [[1, 0]];
        D = [[0]];
        x_0 = [[0], [0]];
        K = [[1.5, 1]];
        r = [[2]];
    } else if (option === 2) {  // Inverted pendulum
        n = 4;
        p = 1;
        q = 2;
        T = 0.05;
        A = [[0, 1, 0, 0], [0, -0.098, 4.72, 0], [0, 0, 0, 1], [0, -0.189, 28.3, 0]];
        B = [[0], [0.98], [0], [1.89]];
        C = [[1, 0, 0, 0], [0, 0, 1, 0]];
        D = [[0], [0]];
        x_0 = [[2], [0], [0.2], [0]];
        K = [[-0.8, -1, 30, 3.5]];
        r = [[0], [0]];
    } else if (option === 3) {  // Double mass spring, lunar lander, satellite orbit, double pendulum, RLC circuit, helicopter
        n = 3;
        p = 1;
        q = 1;
        T = 0.001;
        let R = 10;
        let L = 0.001;
        let J = 0.001;
        let b = 0.001;
        let k = 0.1;
        A = [[0, 1, 0], [0, -b/J, k/J], [0, -k/L, -R/L]];
        B = [[0], [0], [1/L]];
        C = [[1, 0, 0]];
        D = [[0]];
        x_0 = [[0], [0], [0]];
        K = [[60, 3, 0.1]];
        r = [[0.1]];
    } else if (option === 4) {  // Double mass spring
        n = 4;
        p = 2;
        q = 2;
        T = 0.05;
        let k1 = 1;
        let k2 = 2;
        let k3 = 1;
        let d1 = 0.5;
        let d2 = 0.1;
        let d3 = 1;
        let m1 = 1;
        let m2 = 4;
        A = [[0, 1, 0, 0], [-(k1+k2)/m1, -(d1+d2)/m1, k2/m1, d2/m1], [0, 0, 0, 1], [ k2/m2, d2/m2, -(k2+k3)/m2, -(d2+d3)/m2]];
        B = [[0, 0], [1/m1, 0], [0, 0], [0, 1/m2]];
        C = [[1, 0, 0, 0], [0, 0, 1, 0]];
        D = [[0, 0], [0, 0]];
        x_0 = [[0], [0], [0], [0]];
        K = [[5, 2, 5, 1], [5, 1, 5, 2]];
        r = [[1], [-2]];
    } else if (option === 5) {  // Helicopter (pitch, elevation, travel)
        n = 6;
        p = 2;
        q = 3;
        T = 0.05;

        let Kf = 0.15;
        let lp = 0.18;
        let lc = 0.5;
        let lh = 0.64;
        let g = 10;
        let mc = 0.73;
        let mp = 1.81;
        let L1 = Kf * lp;
        let L2 = g*(mc*lc - 2*mp*lh);
        let L3 = Kf*lh;
        let Jl = 0.83;
        let Jp = 0.034;
        let Je = 0.83;
        A = [[0, 1, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 1, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1], [L2/Jl, 0, 0, 0, 0, 0]];
        B = [[0, 0], [0, L1/Jp], [0, 0], [L3/Je, 0], [0, 0], [0, 0]];
        C = [[1, 0, 0, 0, 0, 0], [0, 0, 1, 0 ,0, 0], [0, 0, 0, 0 ,1, 0]];
        D = [[0, 0], [0, 0], [0, 0]];
        x_0 = [[0], [0], [0], [0], [0], [2]];
        K = [[0, 0, 5, 10, 0, 0], [50, 10, 0, 0, -1, -5]];
        r = [[0.1], [0.2], [0]];
    }  // lunar lander, satellite orbit, double pendulum, RLC circuit
    else if (option === 6) {
        let k = 0.113; //LEO*100
        n = 6;
        p = 3;
        q = 3;
        T = 0.05;

        A = [[0, 1, 0, 0, 0, 0], [3*k*k, 0, 0, 2*k, 0, 0], [0, 0, 0, 1, 0, 0], [0, -2*k, 0, 0, 0, 0], [0, 0, 0, 0, 0, 1], [0, 0, 0, 0, -k*k, 0]];
        B = [[0, 0, 0], [1, 0, 0], [0, 0, 0], [0, 1, 0], [0, 0, 0], [0, 0, 1]];
        C = [[1, 0, 0, 0, 0, 0], [0, 0, 1, 0 ,0, 0], [0, 0, 0, 0 ,1, 0]];
        D = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
        x_0 = [[2], [0.1], [0.1], [0], [-1], [0.2]];
        K = [[1, 1, 0, 0, 0, 0], [0, 0, 1, 1, 0, 0], [0, 0, 0, 0, 1, 1]];
        r = [[0], [0], [0]];
    }

    $('#n').val(n);
    $('#p').val(p);
    $('#q').val(q);
    $('#T').val(T);
    generateMatrixInputs(n, p, q);

    fillInput(A, "A", n, n);
    fillInput(B, "B", n, p);
    fillInput(C, "C", q, n);
    fillInput(D, "D", q, p);
    fillInput(x_0, "x0", n, 1);
    fillInput(K, "K", p, n);
    fillInput(r, "r", q, 1);
}

function getnpq() {
    let n = Number($('#n').val());
    let p = Number($('#p').val());
    let q = Number($('#q').val());
    if (n < 1 && n > 8) {
        n = 1;
    }
    if (p < 1 && p > 8) {
        p = 1;
    }
    if (q < 1 && q > 8) {
        q = 1;
    }
    return [n,p,q];
}

function getMatrix(A_name, n, m) {
    let A = [];
    for (var i = 0; i < n; ++i) {
        A[i] = [];
        for (var j = 0; j < m; ++j) {
            let val = $("#" + A_name + "-" + i + "-" + j).val();
            if (val === "") {
                val = 0;
            } else {
                val = Number(val);
            }
            A[i][j] = val;
        }
    }
    return math.matrix(A);
}

function getSystem() {
    let npq = getnpq();
    let n = npq[0];
    let p = npq[1];
    let q = npq[2];
    let T = Number($("#T").val());
    let LQR = $("#lqr").is(":checked");
    let OFB = $("#ofb").is(":checked");
    let kalman = $("#kalman").is(":checked");

    let A = getMatrix("A", n, n);
    let B = getMatrix("B", n, p);
    let C = getMatrix("C", q, n);
    let D = getMatrix("D", q, p);
    let x_0 = getMatrix("x0", n, 1);
    let K = getMatrix("K", p, n);
    let Q = getMatrix("Q", n, n);
    let R = getMatrix("R", p, p);
    let L = getMatrix("L", n, q);
    let G = getMatrix("G", n, 1);
    let H = getMatrix("H", q, 1);
    let r = getMatrix("r", q, 1);

    return [A, n, B, p, C, q, D, T, x_0, K, LQR, OFB, G, H, kalman, L, Q, R, r];
}

function generateText(system) {
    let text = " \\begin{equation} \\mathbf{A} = \\begin{bmatrix}";
    for (var i = 0; i < system.n; ++i) {
        for (var j = 0; j < system.n; ++j) {
            text += math.round(math.subset(system.A, math.index(i, j)), 2);
            if (j < system.n-1) text += " & ";
        }
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\quad ";

    text += "\\mathbf{B} = \\begin{bmatrix} ";
    for (var i = 0; i < system.n; ++i) {
        for (var j = 0; j < system.p; ++j) {
            text += math.round(math.subset(system.B, math.index(i, j)), 2);
            if (j < system.p-1) text += " & ";
        }
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\quad ";

    text += "\\mathbf{C} = \\begin{bmatrix} ";
    for (var i = 0; i < system.q; ++i) {
        for (var j = 0; j < system.n; ++j) {
            text += math.round(math.subset(system.C, math.index(i, j)), 2);
            if (j < system.n-1) text += " & ";
        }
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\quad ";

    text += "\\mathbf{D} = \\begin{bmatrix} ";
    for (var i = 0; i < system.q; ++i) {
        for (var j = 0; j < system.p; ++j) {
            text += math.round(math.subset(system.D, math.index(i, j)), 2);
            if (j < system.p-1) text += " & ";
        }
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\\\ ";

    text += "\\mathbf{x}_0 = \\begin{bmatrix} ";
    for (var i = 0; i < system.n; ++i) {
        text += math.round(system.x_0._data[i], 2);
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\quad ";

    text += "\\mathbf{K} = \\begin{bmatrix} ";
    for (var i = 0; i < system.p; ++i) {
        for (var j = 0; j < system.n; ++j) {
            text += math.round(math.subset(system.K, math.index(i, j)), 2);
            if (j < system.n-1) text += " & ";
        }
        text += " \\\\ ";
    }
    text += " \\end{bmatrix}, \\quad ";

    text += "\\mathbf{r} = \\begin{bmatrix} ";
    for (var i = 0; i < system.q; ++i) {
        text += math.round(system.r._data[i], 2);
        text += " \\\\ ";
    }
    text += " \\end{bmatrix} \\end{equation}";

    $("#matrices").html(text);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "matrices"]);

    let textD = "\\begin{aligned} \\mathbf{A}_d = \\begin{bmatrix} ";
    for (var i = 0; i < system.n; ++i) {
        for (var j = 0; j < system.n; ++j) {
            textD += math.round(math.subset(system.A_d, math.index(i, j)),2);
            if (j < system.n - 1) textD += " & ";
        }
        textD += " \\\\ ";
    }
    textD += " \\end{bmatrix}, \\ \\mathbf{B}_d = \\begin{bmatrix}";
    for (var i = 0; i < system.n; ++i) {
        for (var j = 0; j < system.p; ++j) {
            textD += math.round(math.subset(system.B_d, math.index(i, j)),2);
            if (j < system.p - 1) textD += " & ";
        }
        textD += " \\\\ ";
    }
    textD += " \\end{bmatrix} \\end{aligned}";
    $("#Ad_Bd").html(textD);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub, "Ad_Bd"]);
}