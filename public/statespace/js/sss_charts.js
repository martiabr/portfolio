function pickColor(colors) {
    let index = Math.floor(Math.random() * colors.length);
    let color = colors[index];
    colors.splice(index, 1);
    return color;
}

function addPoint(trajectoryCharts) {
    let point = {
        x: Number($("#trajectory-x").val()),
        x_offset: Number($("#trajectory-x-offset").val()),
        y: Number($("#trajectory-y").val()),
        y_offset: Number($("#trajectory-y-offset").val()),
        trace: $("#trajectory-trace").is(":checked"),
        polar: $("#trajectory-polar").is(":checked"),
    };

    if (trajectoryCharts.length === 0 || trajectoryCharts[trajectoryCharts.length - 1].done) {
        trajectoryCharts[trajectoryCharts.length] = {points: [point], done: false};
    } else {
        trajectoryCharts[trajectoryCharts.length - 1].points.push(point);
    }
}

function trajectoryDone(trajectoryCharts) {
    trajectoryCharts[trajectoryCharts.length - 1].done = true;
}

function addPlot(trajectoryChart, index, system, colors) {
    $("#plots").append("<div class='col-6'><canvas id='trajectory-" + index + "'></canvas></div>");
    let context = initContext('trajectory-' + index, '2d');

    trajectoryChart.chart = new Chart(context, {  // Initialize chart
        type: 'scatter',
        options: {
            tooltips: {
                enabled: false
            },
            animation: {
                duration: 0, // general animation time
            },
            hover: {
                animationDuration: 0, // duration of animations when hovering an item
            },
            responsiveAnimationDuration: 0, // animation duration after a resize
            events: [],
            scales: {
                yAxes: [{
                    ticks: {
                        suggestedMin: -0.25,
                        suggestedMax: 0.25
                    }
                }],
                xAxes: [{
                    ticks: {
                        suggestedMin: -0.25,
                        suggestedMax: 0.25
                    }
                }]
            }
        },
        data: {
            datasets: []
        }
    });

    for (let i = 0; i < trajectoryChart.points.length; ++i) {
        let point = trajectoryChart.points[i];
        let color = pickColor(colors);

        let x = 0;  // Find x and y value for each point
        if (point.x >= 0) {
            if (point.polar) {
                x = (system.x[0]._data[point.x] + point.x_offset) * math.cos(system.x[0]._data[point.y] + point.y_offset*math.pi/180);
            } else {
                x = system.x[0]._data[point.x] + point.x_offset;
            }
        }
        let y = 0;
        if (point.y >= 0) {
            if (point.polar) {
                y = (system.x[0]._data[point.x] + point.x_offset) * math.sin(system.x[0]._data[point.y] + point.y_offset*math.pi/180);
            } else {
                y = system.x[0]._data[point.y] + point.y_offset;
            }
        }

        let label = "";  // Find label for each point
        if (point.x === -1) {
            if (point.y !== -1) {
                label = "Plot of x" + point.y;
            }
        } else if (point.y === -1) {
            label = "Plot of x" + point.x;
        } else {
            label = "Plot of (x" + point.x + ", x" + point.y + ")";
        }

        trajectoryChart.chart.data.datasets[i] = {  // Set data for each point
            data: [{x: x, y: y}],
            backgroundColor: color,
            pointRadius: [4],
            label: label,
        };

        if (point.trace) {  // Set trace for points with trace
            trajectoryChart.chart.data.datasets[i].borderColor = color;
            trajectoryChart.chart.data.datasets[i].borderWidth = 2;
            trajectoryChart.chart.data.datasets[i].showLine = true;
            trajectoryChart.chart.data.datasets[i].fill = false;
        }
    }

    return trajectoryChart;
}

function initTrajectoryCharts(trajectoryCharts, system) {
    let colors = ["#2ecc71", "#3498db", "#9b59b6", "#e67e22", "#e74c3c", "#e1b12c", "#273c75"];

    for (let i = 0; i < trajectoryCharts.length; ++i) {
        trajectoryCharts[i] = addPlot(trajectoryCharts[i], i, system, colors);
    }
    return trajectoryCharts;
}

function initStateTimeCharts() {
    let charts = [];
    let colors = ["#2ecc71", "#3498db", "#9b59b6", "#e67e22", "#e74c3c", "#e1b12c", "#273c75"];
    for (let i = 0; i < system.n; ++i) {
        if ($('#state-time-plot-check-' + i).is(":checked"))  {
            $("#plots").append("<div class='col-6'><canvas id='plot-" + i + "'></canvas></div>");
            let context = initContext('plot-' + i, '2d');

            let color = pickColor(colors);

            let chart = new Chart(context, {
                type: 'line',
                options: {
                    elements: {
                        point: {
                            radius: 0
                        }
                    },
                    tooltips: {
                        enabled: false
                    },
                    animation: {
                        duration: 0, // general animation time
                    },
                    hover: {
                        animationDuration: 0, // duration of animations when hovering an item
                    },
                    responsiveAnimationDuration: 0, // animation duration after a resize
                    events: [],
                },
                data: {
                    labels: ["0"],
                    datasets: [{
                        label: "Plot of x" + i,
                        fill: false,
                        data: [system.x[0]._data[i]],
                        borderColor: color,
                        borderWidth: 2,
                    }]
                }
            });
            if (system.OFB) {
                chart.data.datasets[0].borderWidth = 1;
                chart.data.datasets[0].borderColor = "#808080";
                chart.data.datasets[0].borderDash = [5,5];
                chart.data.datasets.push({
                    label: "Plot of y" + i,
                    fill: false,
                    data: [system.y[0]._data[i]],
                    borderColor: color,
                    borderWidth: 2,
                    lineTension: 0
                });
            }
            charts.push(chart);
        }

        else {
            charts.push(null);
        }
    }

    return charts;
}

function addData(chart, data, time) {
    chart.data.labels.push(time);
    for (let i = 0; i < data.length; ++i) {
        chart.data.datasets[i].data.push(data[i]);
    }
    chart.update(0);
}

function updateTrajectory(chart_obj, system) {
    for (let i = 0; i < chart_obj.points.length; ++i) {
        let point = chart_obj.points[i];

        let x = 0;
        if (point.x !== -1) {
            x = Number(system.x_k._data[point.x]);
        }
        let y = 0;
        if (point.y !== -1) {
            y = Number(system.x_k._data[point.y]);
        }

        if (point.polar) {
            let r = x;
            let theta = y;
            x = (r + point.x_offset) * math.cos(theta + point.y_offset*math.pi/180);
            y = (r + point.x_offset) * math.sin(theta + point.y_offset*math.pi/180);
        } else {
            x += point.x_offset;
            y += point.y_offset;
        }

        chart_obj.chart.data.datasets[i].data.push({x: x, y: y});

        chart_obj.chart.data.datasets[i].pointRadius[chart_obj.chart.data.datasets[i].pointRadius.length - 1] = 0;
        chart_obj.chart.data.datasets[i].pointRadius.push(4);

        chart_obj.chart.update();
    }
}