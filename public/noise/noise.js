
function setup() {
    createCanvas(windowWidth, windowHeight);

    deltaAngle = 5/360 * (TWO_PI);
    center = createVector(windowWidth/2, windowHeight/2);
    radiusMax = 250;
    radiusMin = 20;
    radiusOffsetScale = 150;
    t = 0;
    dt = 0.003;
    a = 0;
    da = 0.0075;
    noiseMax = 1.5;
    circles = 25;
}

function draw() {
    clear();
    noFill();
    strokeWeight(1.5);
    stroke(60, 10, 70);

    for (let i = 0; i < circles; ++i) {
        beginShape();
        for (let angle = 0; angle <= TWO_PI; angle += deltaAngle) {
            let angleOff = map(i, 0, circles - 1, -1, 1);
            let xoff = map(cos(angle + angleOff - a), -1, 1, 0, noiseMax);
            let yoff = map(sin(angle + angleOff - a), -1, 1, 0, noiseMax);
            let r = map(i, 0, circles - 1, radiusMin, radiusMax);
            let randRadius = r + radiusOffsetScale * noise(xoff, yoff, t)+30*sin(15*t);
            let x = center.x + randRadius * sin(angle);
            let y = center.y + randRadius * cos(angle);
            let d = 80*exp(-0.0002*(sq(x-mouseX)+sq(y-mouseY)));
            x += d;
            y += d;
            vertex(x, y);
        }
        endShape();
    }

    t += dt;
    a += da;
}