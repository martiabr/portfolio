## MART

This is a repository for all my js projects, which can be found live at https://martinbrandt.no.
Contents:

- LTI state space system simulation
- 2D wave equation simulation in threej
- Gauss-Newton nonlinear regression calculator
- Marto'Clock - a simple timer for tracking productivity
- Boid simulation
- Conformal image mapper on canvas
- 3D randomly generated terrain simulation in threejs
- Random walk test in threejs
- Lissajous test on canvas
- Polygon drawing on canvas inspired by Nonagon Infinity
- Isometric animation
- Icosahedron test

